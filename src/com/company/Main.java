package com.company;

import java.time.Instant;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

import static java.lang.Thread.sleep;
import static java.util.concurrent.CompletableFuture.supplyAsync;

public class Main {

    public static void main(String[] args) {

        final int sleepTime = 5000;

        ExecutorService executorService = Executors.newFixedThreadPool(3);

        CompletableFuture<Integer> integerCompletableFuture = supplyAsync(getIntegerSupplier(sleepTime, 42), executorService);
//                .thenApplyAsync(v -> {
//                    System.out.printf("[%s] [%s], thenApply to value %d after %d msec%n", Instant.now(), Thread.currentThread().getName(), v, sleepTime);
//                    return v;
//                }, executorService);

        CompletableFuture<Object> combined = integerCompletableFuture
                .thenCombineAsync(supplyAsync(getIntegerSupplier(sleepTime, 666)), (a, b) -> {
            System.out.printf("[%s] [%s]: Combined %d and %d%n", Instant.now(), Thread.currentThread().getName(), a, b);
            return null;
        });

        combined.join();

        executorService.shutdown();

        System.out.printf("[%s] [%s]: FINISHED%n", Instant.now(), Thread.currentThread().getName());
    }

    private static Supplier<Integer> getIntegerSupplier(int sleepTime, int returnResult) {
        return () -> {
            System.out.printf("[%s] [%s], value %d, sleep %d msec%n", Instant.now(), Thread.currentThread().getName(), returnResult, sleepTime);
            try {
                sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.printf("[%s] [%s], value %d, woke up after %d msec%n", Instant.now(), Thread.currentThread().getName(), returnResult, sleepTime);
            return returnResult;
        };
    }
}
