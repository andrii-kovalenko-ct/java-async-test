package com.company;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;
import static java.time.Instant.now;
import static java.util.concurrent.CompletableFuture.supplyAsync;

public class MainCombine {

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            ExecutorService executor = Executors.newFixedThreadPool(3);

            CompletableFuture<Integer> firstFuture = supplyAsync(() -> {
                final int returnValue = 42;
                System.out.printf("%s [%35s], stage 1 (custom pool) returning %3d: before sleeping%n", now(), currentThread().getName(), returnValue);
                try {
                    sleep(1000); // play with this value to verify where thenCombineAsync is executed
                } catch (InterruptedException e) {
                }
                System.out.printf("%s [%35s], stage 1 (custom pool) returning %3d: after sleeping%n", now(), currentThread().getName(), returnValue);
                return returnValue;
            }, executor);

            CompletableFuture<Integer> secondFuture = supplyAsync(() -> {
                final int returnValue = 666;
                System.out.printf("%s [%35s], stage 2 (common ForkJoinPool) returning %3d: before sleeping%n", now(), currentThread().getName(), returnValue);
                try {
                    sleep(1000); // play with this value to verify where thenCombineAsync is executed
                } catch (InterruptedException e) {
                }
                System.out.printf("%s [%35s], stage 2 (common ForkJoinPool), returning %3d: after sleeping%n", now(), currentThread().getName(), returnValue);
                return returnValue;
            });

            // thenCombine might be arbitrary executed on common or custom pool
            CompletableFuture<Integer> combinedFuture = firstFuture
                    .thenCombine(secondFuture, (f, s) -> {
                        // actually, not really "arbitrary": it is always executed on that one of two threads,
                        // which completed later, e.g. which called CompletableFuture#complete(T) the last
                        // -> after that combine function is executed in the same thread.
                        // As an example:
                        //   1) if fist future sleeps longer - the combine function is called always executed in "executor" pool
                        //   2) if second future sleeps longer - the combine function is called always executed in common FJP
                        //   3) when fist and second futures sleep the same time - the combine function is called arbitrary on "executor" pool or common FJP, but always on that one, which finished later!
                        System.out.printf("%s [%35s], combined future: %d and %d (arbitrary executed on the custom pool or common ForkJoinPool)%n", now(), currentThread().getName(), f, s);
                        return f + s;
                    })
                    .thenApply(val -> {
                        System.out.printf("%s [%35s], then apply: %d (always executed on the same thread, where thenCombine executed%n", now(), currentThread().getName(), val);
                        return val;
                    });

            // thenCombineAsync always executed on common ForkJoinPool
            CompletableFuture<Integer> combinedAsyncFuture = firstFuture
                    .thenCombineAsync(secondFuture, (f, s) -> {
                        System.out.printf("%s [%35s], combined future Async: %d and %d (always executed on common ForkJoinPool%n", now(), currentThread().getName(), f, s);
                        return f + s;
                    })
                    .thenApply(val -> {
                        System.out.printf("%s [%35s], then apply: %d (always executed on the same thread, where thenCombineAsync executed%n", now(), currentThread().getName(), val);
                        return val;
                    });

            combinedFuture.join();
            combinedAsyncFuture.join();
            executor.shutdown();

            System.out.printf("%n%n");
        }

        System.out.printf("%s [%35s]: APPLICATION COMPLETED%n", now(), currentThread().getName());
    }
}
