package com.company;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;
import static java.time.Instant.now;
import static java.util.concurrent.CompletableFuture.supplyAsync;

@SuppressWarnings("Duplicates")
public class ThenCombine3WithAsyncAndAsync {

    // Here we create futures with async, and later use thenCombineAsync:
    // 1. thenCombineAsync without executor
    // 2. thenCombineAsync with executor

    public static void main(String[] args) {
        ExecutorService executor1 = Executors.newFixedThreadPool(3);
        ExecutorService executor2 = Executors.newFixedThreadPool(3);

        final int TIMES = 1;
        for (int i = 0; i < TIMES; i++) {





            CompletableFuture<Integer> firstFuture = supplyAsync(() -> {
                final int returnValue = 42;
                System.out.printf("%s [%35s], stage 1 returning %3d: before sleeping%n", now(), currentThread().getName(), returnValue);
                try {
                    sleep(1000); // play with this value to verify where thenCombineAsync is executed
                } catch (InterruptedException e) {
                }
                System.out.printf("%s [%35s], stage 1 returning %3d: after sleeping%n", now(), currentThread().getName(), returnValue);
                return returnValue;
            }, executor1);

            CompletableFuture<Integer> secondFuture = supplyAsync(() -> {
                final int returnValue = 666;
                System.out.printf("%s [%35s], stage 2 returning %3d: before sleeping%n", now(), currentThread().getName(), returnValue);
                try {
                    sleep(1000); // play with this value to verify where thenCombineAsync is executed
                } catch (InterruptedException e) {
                }
                System.out.printf("%s [%35s], stage 2, returning %3d: after sleeping%n", now(), currentThread().getName(), returnValue);
                return returnValue;
            }, executor2);




            CompletableFuture<Integer> combinedFuture1 = firstFuture
                    .thenCombineAsync(secondFuture, (f, s) -> {
                        System.out.printf("%s [%35s], combined future 1: %d and %d%n", now(), currentThread().getName(), f, s);
                        return f + s;
                    }/*, executor1*//*, executor2*/);

            CompletableFuture<Integer> combinedFuture2 = secondFuture
                    .thenCombineAsync(firstFuture, (s, f) -> {
                        System.out.printf("%s [%35s], combined future 2: %d and %d%n", now(), currentThread().getName(), f, s);
                        return f + s;
                    }/*, executor2*//*, executor1*/);







            combinedFuture1.join();
            combinedFuture2.join();


            System.out.printf("%n%n");





            //Actions supplied for dependent completions of non-async methods
            //**may be** performed by the thread that completes the current CompletableFuture,
            //or by any other caller of a completion method.

            //firstFuture.complete(-999999999);
            //secondFuture.complete(-1000);

        }

        executor1.shutdown();
        executor2.shutdown();

        System.out.printf("%s [%35s]: APPLICATION COMPLETED%n", now(), currentThread().getName());
    }
}
