package com.company;

import java.util.concurrent.CompletableFuture;

import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;
import static java.time.Instant.now;
import static java.util.concurrent.CompletableFuture.supplyAsync;

@SuppressWarnings("Duplicates")
public class ThenCombine1WithoutAsync {

//Actions supplied for dependent completions of non-async methods
//**may be** performed by the thread that completes the current CompletableFuture,
//or by any other caller of a completion method.

    // 1. Show example when both stages are completing the same time
    // 2. Make TIMES 10
    // 3. Example when one stage completes later (500 and 1000)
    // 4. Example when stages are completed anticipatorly in main thread
    // 5. Example when stages are completed anticipatorly in dedicated threads

    public static void main(String[] args) throws InterruptedException {
        final int TIMES = 1;
        for (int i = 0; i < TIMES; i++) {




            CompletableFuture<Integer> firstFuture = supplyAsync(() -> {
                final int returnValue = 42;
                System.out.printf("%s [%35s], stage 1 returning %3d: before sleeping%n", now(), currentThread().getName(), returnValue);
                try {
                    sleep(1000); // play with this value to verify where thenCombineAsync is executed
                } catch (InterruptedException e) {
                }
                System.out.printf("%s [%35s], stage 1 returning %3d: after sleeping%n", now(), currentThread().getName(), returnValue);
                return returnValue;
            });

            CompletableFuture<Integer> secondFuture = supplyAsync(() -> {
                final int returnValue = 666;
                System.out.printf("%s [%35s], stage 2 returning %3d: before sleeping%n", now(), currentThread().getName(), returnValue);
                try {
                    sleep(1000); // play with this value to verify where thenCombineAsync is executed
                } catch (InterruptedException e) {
                }
                System.out.printf("%s [%35s], stage 2, returning %3d: after sleeping%n", now(), currentThread().getName(), returnValue);
                return returnValue;
            });

            CompletableFuture<Integer> combinedFuture1 = firstFuture
                    .thenCombine(secondFuture, (f, s) -> {
                        System.out.printf("%s [%35s], combined future 1: %d and %d%n", now(), currentThread().getName(), f, s);
                        return f + s;
                    });

            CompletableFuture<Integer> combinedFuture2 = secondFuture
                    .thenCombine(firstFuture, (s, f) -> {
                        System.out.printf("%s [%35s], combined future 2: %d and %d%n", now(), currentThread().getName(), f, s);
                        return f + s;
                    });

            combinedFuture1.join();
            combinedFuture2.join();

            System.out.printf("%n%n");

            //Actions supplied for dependent completions of non-async methods
            //**may be** performed by the thread that completes the current CompletableFuture,
            //or by any other caller of a completion method.

            // 2.
            //            firstFuture.complete(-999999999);
            //            secondFuture.complete(-1000);

            // 3.
            //            sleep(500); // try without :)
            //            new Thread(() -> firstFuture.complete(555), "CustomCompleteTread1").start();
            //            new Thread(() -> secondFuture.complete(666), "CustomCompleteTread2").start();

        }

        System.out.printf("%n%n");

        System.out.printf("%s [%35s]: APPLICATION COMPLETED%n", now(), currentThread().getName());
    }
}
