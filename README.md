# java-async-test

Test examples for commercetools PS team about Java async execution
(CompletionStage/CompletableFuture)

https://wiki.commercetools.com/display/PS/Java+Async+Execution%3A+CompletionStage%2C+CompletableFuture

## Examples:

 1. without async: combined future is always executed in
 _this stage's default asynchronous execution facility_

 **Note: without async, according to the documentation,
 there is no guarantee to complete in the same thread!!!**

 https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletableFuture.html

 [ThenCombine1WithoutAsync](/src/ThenCombine1WithoutAsync.java)

   - Actions supplied for dependent completions of non-async methods
     **may be** performed by the thread that completes the current
     CompletableFuture, or by any other caller of a completion method.

   - All async methods without an explicit Executor argument
     are performed using the **ForkJoinPool.commonPool()**
     (unless it does not support a parallelism level of at least two,
     in which case, a new Thread is created to run each task).

   - **Note:** now any other "thread" references in the documentation!!!

 2. With async: [ThenCombine2WithAsyncAndSync](/src/ThenCombine2WithAsyncAndSync.java) 
 and [ThenCombine3WithAsyncAndAsync](/src/ThenCombine3WithAsyncAndAsync.java)

  2.1. complete both stages at averagely same time - the combined future
  is executed at arbitrary thread

  2.2. complete first stage faster - the combined future is executed at
  second stage pool (facility)

  2.3. complete second stage faster - the combined future is executed
  at first stage pool (facility) the cases 2 and 3 don't guarantee such
  behavior, it is just guaranteed that it is
  "executed using this stage's default asynchronous execution facility"
